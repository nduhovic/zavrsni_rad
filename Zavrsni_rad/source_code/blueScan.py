from pubnub import Pubnub
import subprocess
import re

PUBLISHKEY = 'pub-c-cf2b5cf7-2284-4bd0-9acc-c08a460f9505'
SUBSCRIBEKEY = 'sub-c-0bd62a7e-132f-11e6-875d-0619f8945a4f'
SECRETKEY = 'sec-c-ZDIwMjRmYWEtY2FlYS00MTk2LWEwZmItNjk3NjAxYTMxNTg5'
CHANNEL = "test"
pubnub = Pubnub(publish_key=PUBLISHKEY, subscribe_key=SUBSCRIBEKEY)     


def f():
    p = subprocess.Popen(['hcitool', 'scan'], stdout=subprocess.PIPE)   
    for line in p.stdout:                                               
        isValid = re.search(r'..:..:..:..:..:..*', line)                
        if isValid:
            values = line.split('\t')                                   
            values[-1] = values[-1].strip()                             
            data = {                                                    
                'tip': 'bluetooth',
                'BT_ADDR(MAC)': values[1],
                'device name': values[2]
            }
            publish = pubnub.publish(channel=CHANNEL, message=data)     


while True:                                                          
    f()
