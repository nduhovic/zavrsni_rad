from pubnub import Pubnub
import subprocess
import re
from time import time


keepTime = 10                                      
filterList = []
filterListElement = ['', '']

PUBLISHKEY = 'pub-c-cf2b5cf7-2284-4bd0-9acc-c08a460f9505'
SUBSCRIBEKEY = 'sub-c-0bd62a7e-132f-11e6-875d-0619f8945a4f'
SECRETKEY= 'sec-c-ZDIwMjRmYWEtY2FlYS00MTk2LWEwZmItNjk3NjAxYTMxNTg5'
CHANNEL = "test"
pubnub = Pubnub(publish_key=PUBLISHKEY, subscribe_key=SUBSCRIBEKEY)


cmd = ['/root/Desktop/zavrsni/main.sh'] #zamijeniti tocnim putem do skripte main.sh
p = subprocess.Popen(cmd, stdout=subprocess.PIPE)

for line in p.stdout:
    isValid = re.search(r'..:..:..:..:..:..  ', line)  
    if isValid:
        line = line.replace("  ", "~")
        values = re.split('~+', line, maxsplit=7)    
        values[-1] = values[-1].strip()              
        if len(values) == 6:
            values.append('/')
        t = time()
        values.append(t)
        flag = False
        j = 0

        for i in range(len(filterList)):
            if filterList[j][0] == values[1]:
                flag = True
            if t - filterList[j][1] >= keepTime:
                filterList.remove(filterList[j])
                j -= 1
            j += 1

        if not flag:                                
            data = {
                'tip': 'WiFi',
                'AP MAC': values[0],
                'device MAC': values[1],
                'signal strength db': values[2],
                'rate': values[3],
                'lost': values[4],
                'frames': values[5],
                'probe': values[6],
				'trenutno u dometu': len(filterList)
            }

            publish = pubnub.publish(channel=CHANNEL, message=data)


            filterListElement[0] = values[1]
            filterListElement[1] = values[-1]
            filterList.append((filterListElement[0], filterListElement[1]))
            print len(filterList), data